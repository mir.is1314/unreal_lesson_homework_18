﻿
#include <iostream>
using namespace std;

class Stack
{
private:
	int size;

public:
	
	
	void SetSize(int valueSize)
	{
		cout << "Введите размер стека" << endl;
		cin >> valueSize;
		size = valueSize;
	}
	int* Arr = new int[size]; // объявляем динамический массив, пользователь вводит его размер

	void Pop() //достаем последний элемент из стека - объявляем новый массив на размер меньше и переправляем туда данные из старого
	{
		size--;
		int* smallArr = new int[size];
		for (int i = 0; i < size; i++)
		{
			smallArr[i] = Arr[i];
		}
		delete[] Arr;
		Arr = smallArr;
	
	}
	void Push()//добавляем элемент в стек - объявляем новый массив на размер больше и переправляем туда данные из старого
	{
		size++;
		int* bigArr = new int[size];
		for (int i = 0; i < size; i++)
		{
			bigArr[i] = Arr[i];
		}
		delete[] Arr;
		Arr = bigArr;
	}

	void Print()
	{
		cout << "current size = " << '\t' << size << endl;
	}
};


int main()
{
	setlocale(LC_ALL, "ru");
	Stack Test;
	Test.SetSize(7);
	Test.Print();

	Test.Pop();
	Test.Print();

	Test.Push();
	Test.Print();

	return 0;
}